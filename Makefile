# Makefile for passgen
#

# TAG = $(shell git describe --tags --abbrev=0)

APP_NAME := passgen
VERSION  := 1.0.0.1

GO      := go
LDFLAGS := "-s -w -X main.Version=${VERSION} -X main.AppName=${APP_NAME}"
LDFLAGS_DEBUG := "-X main.Version=${VERSION} -X main.AppName=${APP_NAME}"

# Directory underwhich this Makefile resides
BUILD_ROOT := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

# Source directory for dnscan cmdline app
SOURCE_DIR := $(BUILD_ROOT)/src

# Default target builds standard binary 
.PHONY: all
all: $(APP_NAME)

# Build stripped binary 
.PHONY: $(APP_NAME)
$(APP_NAME): check-env
	GOOS=$(GOOS); GOARCH=$(GOARCH); $(GO) build -ldflags $(LDFLAGS) -o $(BUILD_ROOT)$(APP_NAME) main.go 

# Build debugging binary 
.PHONY: debug
debug: check-env
	GOOS=$(GOOS); GOARCH=$(GOARCH); $(GO) build -ldflags $(LDFLAGS_DEBUG) -o $(BUILD_ROOT)$(APP_NAME) main.go 

# Run stripped binary
.PHONY: run
run: $(APP_NAME)
	@$(BUILD_ROOT)$(APP_NAME)

# Run debugging binary 
.PHONY: run-debug
run-debug: debug
	@$(BUILD_ROOT)$(APP_NAME)

.PHONY: clean
clean:
	@# Delete TARBALL and related artifacts
	rm -f $(BUILD_ROOT)$(APP_NAME)

.PHONY: check-env
check-env:
ifndef GOOS
    ifeq ($(OS),Windows_NT)
        GOOS ?= windows
    else
        GOOS ?= "$(shell uname -s | tr '[:upper:]' '[:lower:]')"
    endif

    ifeq ($(GOOS),)
        $(error GOOS is undefined)
    endif
endif

ifndef GOARCH
    ifeq ($(OS), Windows_NT)
        ifeq ($(PROCESSOR_ARCHITEW6432),AMD64)
            GOARCH ?= AMD64
        else
            ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
                GOARCH ?= AMD64
            endif
            ifeq ($(PROCESSOR_ARCHITECTURE),x86)
                GOARCH ?= 386
            endif
        endif
    else
        GOARCH ?= "$(shell uname -m | tr '[:upper:]' '[:lower:]')"
    endif
    ifeq ($(GOARCH),)
        $(error GOARCH is undefined)
    endif
endif