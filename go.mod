module bitbucket.org/srozzo/pbdkf2

require (
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	golang.org/x/crypto v0.0.0-20181106171534-e4dc69e5b2fd
)
