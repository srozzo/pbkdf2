package main

import (
	"encoding/hex"
	"reflect"
	"testing"
)

func Test_getPbkdf2(t *testing.T) {
	var bSalt, _ = hex.DecodeString("1b9219c7e2be2bd120acd007e2fadd04")
	var bHash, _ = hex.DecodeString("13e211e871a4a9280f349ba809eca51ad7a4bc66e32cbaa7d06b30dea76f05bc")

	var bSalt2, _ = hex.DecodeString("1b9219c7e2be2bd120acd007e2fadd05")
	var bHash2, _ = hex.DecodeString("d4b00c6b9881fa9ac5f12ef4a578bbb13f894707405a47ea5b5c7dd829877462")

	type args struct {
		password string
		salt     []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		/* Given a specific salt and password we should be able to reproduce the same result providing the number of iterations is constant */
		{"matching",
			args{
				"abc123abc",
				[]byte(bSalt),
			},
			[]byte(bHash),
			false},
		/* Given a the same salt with 1 byte shifted and the same password, we should expect a completely differnt output with the same number of iterations */
		{"matching2",
			args{
				"abc123abc",
				[]byte(bSalt2),
			},
			[]byte(bHash2),
			false},
		{"passwordTooShort",
			args{
				"abc123",
				[]byte{},
			},
			[]byte{},
			true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getPbkdf2(tt.args.password, tt.args.salt)
			if (err != nil) != tt.wantErr {
				t.Errorf("genPbkdf2() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("genPbkdf2() = %v, want %v", got, tt.want)
			}
		})
	}
}
