package main

/* Simple command line binary for generating pbdkf2 hashes */

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"os"

	docopt "github.com/docopt/docopt-go"
	"golang.org/x/crypto/pbkdf2"
)

// AppName is the command line representation of the application name
// Version is the version of the tool
var (
	AppName string
	Version string
)

// VarArgs represents a composite of the arguments passed to the application at init
type VarArgs struct {
	Password string
}

func main() {
	args := getVarArgs()

	// Per NIST 2010 - Salt shall be atleast 128 random bits
	salt, err := genSalt(16)
	if err != nil {
		os.Stderr.WriteString(err.Error())
		os.Exit(1)
	}

	pbkdg2Out, err := getPbkdf2(args.Password, salt)
	if err != nil {
		os.Stderr.WriteString(err.Error())
		os.Exit(1)
	}

	// Print colon delimited salt:hash
	fmt.Println(hex.EncodeToString(salt) + ":" + hex.EncodeToString(pbkdg2Out))
}

// getVarArgs parses the application arguments and generates the usage / help output presented to the user.
func getVarArgs() *VarArgs {
	var (
		args = make(map[string]interface{})
		err  error
	)

	usage := `
Usage:
	` + AppName + ` <password>
	` + AppName + ` --version

Options:
	--version     Show version.`

	// Process arguments. If we run into an issue here, bail out.
	args, err = docopt.Parse(usage, nil, true, Version, false)
	if err != nil {
		errStr := "Error processing arguments: " + err.Error()
		os.Stderr.WriteString(errStr + "\n")
		os.Exit(1)
	}

	varargs := &VarArgs{
		Password: args["<password>"].(string)}

	// Enforce a reasonable password length
	if len(varargs.Password) < 8 {
		errStr := "Password must be atleast 8 characters long"
		os.Stderr.WriteString(errStr + "\n")
		os.Exit(1)
	}

	return varargs
}

// genSalt generates returns a byte array of n random bytes
// Per NIST 2010 - Salt shall be atleast 128 random bits
func genSalt(n int) ([]byte, error) {
	// Create byte array of size n bytes
	salt := make([]byte, n)

	// Fill said array with entropy
	_, err := rand.Read(salt)
	if err != nil {
		return salt, err
	}

	return salt, nil
}

// getPbkdf2 generates a stretched key based on the provided password and salt.
func getPbkdf2(password string, salt []byte) ([]byte, error) {
	// If you're using key stretching, enforce a password length
	if len(password) < 8 {
		return []byte{}, errors.New("Do not use passwords shorter than 8 characters")
	}

	/* Note: If the key length is more than the number of output bytes of the hash
	 * algorithm, the process is repeated twice, slowing down that hashing
	 * perhaps more than you desire. SHA256 gives 32 bytes of output, for
	 * example, so asking for 33 byte will take roughly twice as long as
	 * asking for 32.*/
	keyLen := 32

	// Per NIST 2010 - 10000 iterations minimum should be used (https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-132.pdf)
	// This implementation uses the largest amount beyond the minimum recommend possible without compromising responsiveness.
	// We also pick a obscure number to prevent obvious precomputed tables based on multiples or powers of ten that most people implement.
	iterations := 144729

	// generate stretched key
	hash := pbkdf2.Key([]byte(password), salt, iterations, keyLen, sha256.New)

	return hash, nil
}
