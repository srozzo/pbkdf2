package valpbkdf2

import (
	"encoding/hex"
	"hash"

	"golang.org/x/crypto/pbkdf2"
)

// ValidateKey compares a given password and salt output to the supplied hash
func ValidateKey(password string, salt string, hash string, iterations int, keyLen int, algo func() hash.Hash) (bool, error) {
	// Convert hex salt string to byte array
	s, err := hex.DecodeString(salt)
	if err != nil {
		return false, err
	}

	// generate stretched key
	tHash := pbkdf2.Key([]byte(password), s, iterations, keyLen, algo)
	if err != nil {
		return false, err
	}

	// Compare supplied hash to generated hash
	if hash == hex.EncodeToString(tHash) {
		// We have a match
		return true, nil
	}

	// Did not match
	return false, nil
}
