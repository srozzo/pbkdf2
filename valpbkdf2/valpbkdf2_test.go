package valpbkdf2

import (
	"crypto/sha256"
	"hash"
	"testing"
)

func TestValidateKey(t *testing.T) {
	type args struct {
		password   string
		salt       string
		hash       string
		iterations int
		keyLen     int
		algo       func() hash.Hash
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{"password with matching salt",
			args{
				"abc123abc",
				"1b9219c7e2be2bd120acd007e2fadd04",
				"13e211e871a4a9280f349ba809eca51ad7a4bc66e32cbaa7d06b30dea76f05bc",
				144729,
				32,
				sha256.New,
			},
			true,
			false,
		},
		{"password with mismatching salt",
			args{
				"abc123abc",
				"a34ccd72da3adaa8e59bf5ecee6738d8",
				"ba50940c9999080b29917aed6e4fef17fdb895dec76ac25ee09a5aaa91f558f3",
				144729,
				32,
				sha256.New,
			},
			false,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ValidateKey(tt.args.password, tt.args.salt, tt.args.hash, tt.args.iterations, tt.args.keyLen, tt.args.algo)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ValidateKey() = %v, want %v", got, tt.want)
			}
		})
	}
}
